package com.digitalpointzero2.transportmaps;

/**
 * Created by scjb on 26/8/16.
 */

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class MapsTable {

  // Database table
  public static final String TABLE_MAPS = "maps";
  public static final String COLUMN_ID = "_id";
  public static final String COLUMN_CITY = "city";
  public static final String COLUMN_COUNTRY = "country";
  public static final String COLUMN_FILEPATH = "file_path";

  // Database creation SQL statement
  private static final String DATABASE_CREATE = "create table "
      + TABLE_MAPS
      + "("
      + COLUMN_ID + " integer primary key autoincrement, "
      + COLUMN_CITY + " text not null, "
      + COLUMN_COUNTRY + " text not null,"
      + COLUMN_FILEPATH
      + " text not null"
      + ");";

  public static void onCreate(SQLiteDatabase database) {
    database.execSQL(DATABASE_CREATE);
  }

  public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                               int newVersion) {
    Log.w(MapsTable.class.getName(), "Upgrading database from version "
        + oldVersion + " to " + newVersion
        + ", which will destroy all old data");
    database.execSQL("DROP TABLE IF EXISTS " + TABLE_MAPS);
    onCreate(database);
  }
}
