package com.digitalpointzero2.transportmaps;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.digitalpointzero2.transportmaps.helpers.TransportMapJSONParser;

import java.util.HashMap;

import com.digitalpointzero2.transportmaps.helpers.MapsDatabaseHelper;

/**
 * Created by scjb on 24/8/16.
 */
public class MapsContentProvider extends ContentProvider {

  public static final String TAG = "MapsContentProvider";
  static final String AUTHORITY = "com.example.com.digitalpointzero2.transportmaps";

  // database
  private MapsDatabaseHelper database;

  private static final int MAPS = 10;
  private static final int MAP_ID = 20;
  private static final String BASE_PATH = "maps";
  public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
      + "/" + BASE_PATH);
  private static final UriMatcher sURIMatcher = new UriMatcher(
      UriMatcher.NO_MATCH);
  static {
    sURIMatcher.addURI(AUTHORITY, BASE_PATH, MAPS);
    sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", MAP_ID);
  }

  private TransportMapJSONParser json = new TransportMapJSONParser();

  private final java.util.Map ITEM_MAP = new HashMap<>();

  private Uri mapsUri;

  @Override
  public boolean onCreate() {
    database = new MapsDatabaseHelper(getContext());
    return true;

//
//    try {
//      //Get all maps from Assets
//      AssetManager assetManager = getContext().getAssets();
//      // To get names of all files inside the "Files" folder
//      String[] assetFiles = assetManager.list("maps");
//
//      // parse each file
//      for (String file : assetFiles) {
//        if (file.endsWith(".json")){
//          MapsContent.TransportMap transportMap = json.parse(assetManager.open("maps/" + file));
//          //TODO replace with SQLlite database
//          ITEM_MAP.put(transportMap.id, transportMap);
//
//          ContentValues values = new ContentValues();
//          values.put(MapsTable.COLUMN_CITY, transportMap.id);
//          values.put(MapsTable.COLUMN_COUNTRY, transportMap.geography);
//          values.put(MapsTable.COLUMN_FILEPATH, transportMap.imagePath.toString());
//          mapsUri = getContext().getContentResolver().insert(
//              MapsContentProvider.CONTENT_URI, values);
//        }
//      }
//    } catch (IOException |NullPointerException e) {
//      e.printStackTrace();
//      Log.d(TAG, e.getMessage());
//      return false;
//    }
  }

  @Override
  public synchronized Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

    // Uisng SQLiteQueryBuilder instead of query() method
    SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

    // Set the table
    queryBuilder.setTables(MapsTable.TABLE_MAPS);

    int uriType = sURIMatcher.match(uri);
    switch (uriType) {
      case MAP_ID:
        break;
      case MAPS:
        // adding the ID to the original query
        queryBuilder.appendWhere(MapsTable.COLUMN_ID + "="
            + uri.getLastPathSegment());
      default:
        throw new IllegalArgumentException("Unknown URI: " + uri);
    }

//    //Build projection
//
//    //loop over ITEM_MAP and retrieve matching from selection
//    projection.
//    matrixCursor.addRow(new Object[] { transportMap.id, transportMap.geography, transportMap.imagePath });
//
//    Cursor cursor = matrixCursor.get  ( projection, selection,
//        selectionArgs, null, null, sortOrder);
//    // make sure that potential listeners are getting notified
//    cursor.setNotificationUri(getContext().getContentResolver(), uri);

    SQLiteDatabase db = database.getWritableDatabase();
    Cursor cursor = queryBuilder.query(db, projection, selection,
        selectionArgs, null, null, sortOrder);
    // make sure that potential listeners are getting notified
    cursor.setNotificationUri(getContext().getContentResolver(), uri);

    return cursor;
  }

  @Override
  public synchronized String getType(Uri uri) {
    return null;
  }

  @Override
  public synchronized Uri insert(Uri uri, ContentValues values) {
    int uriType = sURIMatcher.match(uri);
    SQLiteDatabase sqlDB = database.getWritableDatabase();
    long id = 0;
    switch (uriType) {
      case MAPS:
        id = sqlDB.insert(MapsTable.TABLE_MAPS, null, values);
        break;
      default:
        throw new IllegalArgumentException("Unknown URI: " + uri);
    }
    getContext().getContentResolver().notifyChange(uri, null);
    return Uri.parse(BASE_PATH + "/" + id);
  }

  @Override
  public synchronized int delete(Uri uri, String selection, String[] selectionArgs) {
    throw new UnsupportedOperationException();
  }

  @Override
  public synchronized int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
    throw new UnsupportedOperationException();
  }
}
