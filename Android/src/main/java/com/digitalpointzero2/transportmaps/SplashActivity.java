package com.digitalpointzero2.transportmaps;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent i = new Intent(this, MapsListActivity.class);
        startActivity(i);

        //Finish this activity immediately after the next one has started to
        // prevent it from staying on the back stack, so that pressing back in
        // next activity doesn't show this loading screen.
        finish();

    }
}
