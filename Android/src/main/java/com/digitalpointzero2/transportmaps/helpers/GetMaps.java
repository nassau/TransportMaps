package com.digitalpointzero2.transportmaps.helpers;

import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by scjb on 20/5/16.
 */
public class GetMaps extends AsyncTask<String, Void, String> {


  public String doInBackground(String... urls) {
    try {

      String response = null;
      URL url = null;
      try {
        url = new URL("http://54.93.192.62:3000/");
      } catch (MalformedURLException e) {
        e.printStackTrace();
      }
      HttpURLConnection urlConnection = null;
      try {
        urlConnection = (HttpURLConnection) url.openConnection();
      } catch (IOException e) {
        e.printStackTrace();
      }
      try {
        InputStream in = new BufferedInputStream(urlConnection.getInputStream());

        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder result = new StringBuilder();
        String line;
        while((line = reader.readLine()) != null) {
          result.append(line);
        }
        System.out.println(result.toString());
        response = line;
      } catch (IOException e) {
        e.printStackTrace();
      } finally {
        urlConnection.disconnect();
      }

      return response;
    } catch (Exception e) {
      return null;
    }
  }

  protected void onPostExecute(String feed) {
    // TODO: check this.exception
    // TODO: do something with the feed
  }
}