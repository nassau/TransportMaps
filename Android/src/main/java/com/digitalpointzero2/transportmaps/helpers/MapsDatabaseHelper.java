package com.digitalpointzero2.transportmaps.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.digitalpointzero2.transportmaps.MapsTable;

/**
 * Created by scjb on 26/8/16.
 */
public class MapsDatabaseHelper extends SQLiteOpenHelper{

  private static final String DATABASE_NAME = "mapstable.db";
  private static final int DATABASE_VERSION = 1;

  public MapsDatabaseHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  // Method is called during creation of the database
  @Override
  public void onCreate(SQLiteDatabase database) {
    MapsTable.onCreate(database);
  }

  // Method is called during an upgrade of the database,
  // e.g. if you increase the database version
  @Override
  public void onUpgrade(SQLiteDatabase database, int oldVersion,
                        int newVersion) {
    MapsTable.onUpgrade(database, oldVersion, newVersion);
  }
}
