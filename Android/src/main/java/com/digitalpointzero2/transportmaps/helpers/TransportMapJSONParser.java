/**
 * 
 * This is OpenTraining, an Android application for planning your your fitness training.
 * Copyright (C) 2012-2014 Christian Skubich
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package com.digitalpointzero2.transportmaps.helpers;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import com.digitalpointzero2.transportmaps.MapsContent;

public class TransportMapJSONParser extends AbtractJSONParser<MapsContent.TransportMap> {
	/** Tag for logging */
	public static final String TAG = "TransportMapJSONParser";
	
	// JSON Node names
	private static final String TAG_NAME = "name";
  private static final String TAG_GEOGRAPHY = "geography";
  private static final String TAG_IMAGE = "image";
  private static final String TAG_LICENSE = "license";
	 
	/**
	 * Parses the JSON-String to a {@link MapsContent.TransportMap}.
	 * 
	 * Example for such a .json File:
	 * 
	 *  {
   *    "name"        : "Singapore MRT / LRT System",
   *    "geography"   : "South-East-Asia",
   *    "image"       : "00_System Map (Up to TEL) with DTL2 solid-Dec15.jpg",
   *    "license"     : "Copyright Land Transport Authority of Singapore"
   *  }
   *
   * @param jsonString The String to parse.
   *
   * @return A {@link MapsContent.TransportMap}, null if an Error occurs.
 	 * 
	 */
	@Override
	public MapsContent.TransportMap parse(String jsonString) {
    String name = "(missing name)";
    String geography ="(missing geography)";
    String license ="(missing license)";
    File imagePath;

		try {

      JSONObject workoutObject = new JSONObject(jsonString);

      name = workoutObject.getString(TAG_NAME);
      license = workoutObject.getString(TAG_LICENSE);
      geography = workoutObject.getString(TAG_GEOGRAPHY);
      imagePath = new File(workoutObject.getString(TAG_IMAGE));

      return new MapsContent.TransportMap( name, geography, license, imagePath);

		} catch (JSONException e) {
			Log.e(TAG, "Error during parsing JSON File.", e);
			return null;
		}
  }

	private String[] jsonArrayToStringArray(JSONArray jsonArray){
			return jsonArray.toString().substring(1,jsonArray.toString().length()-1).replaceAll("\"","").split(",");
	}
}
