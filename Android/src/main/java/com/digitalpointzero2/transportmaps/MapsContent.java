package com.digitalpointzero2.transportmaps;

import android.content.res.AssetManager;
import androidx.appcompat.widget.ShareActionProvider;
import android.util.Log;

import com.digitalpointzero2.transportmaps.helpers.TransportMapJSONParser;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Helper class for providing content
 */
public class MapsContent {

  /** Tag for logging */
  public static final String TAG = MapsContent.class.getSimpleName();

  /** The name of the folder that contains example {@link TransportMap} */
  static public final String EXAMPLE_MAP_FOLDER = "example_map";

  /** The name of the folder that contains example {@link TransportMap} */
  static public final String USER_MAP_FOLDER = "maps";

  /**
   * An array of items.
   */
  public static final List<TransportMap> ITEMS = new ArrayList<>();

  /**
   * A map of items, by ID.
   */
  public static final java.util.Map ITEM_MAP = new HashMap<>();


  static {

    //Get all maps from Assets
    AssetManager assetManager = MapsListActivity.getContext().getAssets();
    // To get names of all files inside the "Files" folder
    try {
      String[] assetFiles = assetManager.list("maps");
      //add user maps
//      String[] files = concatenate(assetFiles, getUserMaps());

      // parse each file
      for (String file : assetFiles) {
        if (file.endsWith(".json")){
          MapsContent.TransportMap transportMap = new TransportMapJSONParser().parse(assetManager.open("maps/" + file));
          addItem(transportMap);
        }
      }
    } catch (IOException e1) {
      e1.printStackTrace();
    }
  }

  private static void addItem(TransportMap item) {
    ITEMS.add(item);
    ITEM_MAP.put(item.id, item);
  }


  /**
   * An item representing a single transport map content.
   */
  public static class TransportMap {
    public final String id;
    public final String country;
    public final String license;
    public File imagePath;

    public TransportMap(String id, String country, String license, File imagePath) {
      this.id = id;
      this.country = country;
      this.license = license;
      this.imagePath = imagePath;
    }

    @Override
    public String toString() {
      return id;
    }
  }

  /**
   * Read External Storage app directory
   */
  private static String[] getUserMaps() {
    // find files that end with ".json"
    File userMapDir = new File(MapsListActivity.getContext().getExternalFilesDir(null) + "/" + USER_MAP_FOLDER);
    String files[] = userMapDir.list(new FilenameFilter() {
      @Override
      public boolean accept(File dir, String filename) {
        return filename.endsWith(".json")
//            && dir.equals(USER_MAP_FOLDER)
            && !filename.equals(ShareActionProvider.DEFAULT_SHARE_HISTORY_FILE_NAME);
      }
    });


    if(files == null){
      Log.d(TAG, "Searched" + MapsListActivity.getContext().getExternalFilesDir(null) + "/" + USER_MAP_FOLDER);
      Log.d(TAG, "No user maps found, only using built-in ones");
      //No user maps are found, copy example files to external storage
      //copyExampleMap();
      //prevent NullPointerException, initialize a zero length array
      files = new String[0];
    }else {
      Log.v(TAG, "Read " + files.length + " user maps");
    }
    return files;
  }


  public static <T> T[] concatenate (T[] a, T[] b) {
    int aLen = a.length;
    int bLen = b.length;

    @SuppressWarnings("unchecked")
    T[] c = (T[]) Array.newInstance(a.getClass().getComponentType(), aLen + bLen);
    System.arraycopy(a, 0, c, 0, aLen);
    System.arraycopy(b, 0, c, aLen, bLen);

    return c;
  }


//  /**
//   * Copies the example map to the file system.
//   */
//  private static void copyExampleMap() {
//    try {
//      String[] exampleMaps = getContext().getAssets().list(EXAMPLE_MAP_FOLDER);
//      for (String file : exampleMaps) {
//        InputStream in = null;
//        OutputStream out = null;
//        if (Environment.getExternalStorageState().startsWith(Environment.MEDIA_MOUNTED)) {
//          // External Storage is mounted, go on
//          try {
//            in = getContext().getAssets().open(EXAMPLE_MAP_FOLDER + "/" + file);
//            out = new FileOutputStream(getContext()
//                .getExternalFilesDir(null) + "/" + USER_MAP_FOLDER + "/" + file);
//            // copy file
//            byte[] buffer = new byte[1024];
//            int read;
//            while ((read = in.read(buffer)) != -1) {
//              out.write(buffer, 0, read);
//            }
//
//            in.close();
//            in = null;
//            out.flush();
//            out.close();
//            out = null;
//          } catch (IOException e) {
//            Log.e(TAG, "Failed to copy example map files: " + file, e);
//          }
//        }
//      }
//
//      Log.v(TAG, "Copied example map to" + getContext()
//          .getExternalFilesDir(null) + "/" + USER_MAP_FOLDER + "/");
//    } catch (IOException e) {
//      Log.e(TAG, "Copying example maps failed", e);
//    }
//  }

}