package com.digitalpointzero2.transportmaps;

import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

/**
 * A fragment representing a single maps detail screen.
 * This fragment is either contained in a {@link MapsListActivity}
 * in two-pane mode (on tablets) or a {@link MapsDetailActivity}
 * on handsets.
 */
public class MapsDetailFragment extends Fragment {
  /**
   * The fragment argument representing the item ID that this fragment
   * represents.
   */
  public static final String ARG_ITEM_ID = "item_id";

  /**
   * The dummy content this fragment is presenting.
   */
  private MapsContent.TransportMap mItem;

  /**
   * Mandatory empty constructor for the fragment manager to instantiate the
   * fragment (e.g. upon screen orientation changes).
   */
  public MapsDetailFragment() {

  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);

    if (getArguments().containsKey(ARG_ITEM_ID)) {
      // Load the dummy content specified by the fragment
      // arguments. In a real-world scenario, use a Loader
      // to load content from a content provider.

      mItem = (MapsContent.TransportMap) MapsContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
    }
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    // Call the superclass so it can save the view hierarchy state
    super.onSaveInstanceState(outState);
    // Save our own state now
    outState.putString(ARG_ITEM_ID, mItem.id);
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    if (savedInstanceState != null) {
      //Restore the fragment's state here
      mItem = (MapsContent.TransportMap) MapsContent.ITEM_MAP.get(savedInstanceState.getString(ARG_ITEM_ID));
    }

    AppCompatActivity activity = (AppCompatActivity) this.getActivity();

    Toolbar toolbar = (Toolbar) activity.findViewById(R.id.detail_toolbar);
    //Only in two-pane layout is the toolbar available
    if (toolbar != null) {
      activity.setSupportActionBar(toolbar);
      //TODO: mItem.id is empty after rotating
      toolbar.setTitle(mItem.id);
    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View rootView = inflater.inflate(com.digitalpointzero2.transportmaps.R.layout.maps_detail, container, false);

    // Show the dummy content as text in a TextView.
    if (mItem != null) {
//      ((TextView) rootView.findViewById(R.id.maps_detail)).setText(mItem.license);


//            ((ImageView) rootView.findViewById(R.id.maps_image)).setImageBitmap(map);
      SubsamplingScaleImageView imageView = ((SubsamplingScaleImageView) rootView.findViewById(com.digitalpointzero2.transportmaps.R.id.imageView));
      imageView.setImage(ImageSource.asset("maps/" + mItem.imagePath));
      imageView.setPanLimit(SubsamplingScaleImageView.PAN_LIMIT_CENTER);
      imageView.setBackgroundColor(Color.BLACK);
    }

    return rootView;
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    inflater.inflate(com.digitalpointzero2.transportmaps.R.menu.menu_mapdetail, menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {

      case com.digitalpointzero2.transportmaps.R.id.action_license:
        // User chose the "Licence" item, show map licence...
        showSnackbar();
        return true;

      default:
        // If we got here, the user's action was not recognized.
        // Invoke the superclass to handle it.
        return super.onOptionsItemSelected(item);
    }
  }

  void showSnackbar(){
    Snackbar.make(getView(), mItem.license, Snackbar.LENGTH_LONG)
        .setAction("Action", null).show();
  }


}
