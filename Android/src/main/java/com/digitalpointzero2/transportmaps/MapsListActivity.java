package com.digitalpointzero2.transportmaps;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.view.MenuItemCompat;
import androidx.cursoradapter.widget.SimpleCursorAdapter;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * An activity representing a list of maps. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link MapsDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class MapsListActivity extends AppCompatActivity {

  /**
   * Whether or not the activity is in two-pane mode, i.e. running on a tablet
   * device.
   */
  private boolean mTwoPane;

  private static Context mContext;

  View recyclerView;

  private SimpleItemRecyclerViewAdapter mAdapter;

  // private Cursor cursor;
  private SimpleCursorAdapter adapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(com.digitalpointzero2.transportmaps.R.layout.activity_maps_list);
    mContext = this;

//    CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
    Toolbar appBar = (Toolbar) findViewById(com.digitalpointzero2.transportmaps.R.id.app_bar);
    if (appBar != null) {
      setSupportActionBar(appBar);
      getSupportActionBar().setTitle(getTitle());
//      appBarLayout.setTitle(getTitle());
    }

//    ImageView imageBackdrop = (ImageView) findViewById(R.id.backdrop);
    //TODO find a good image backdrop
//    if(imageBackdrop !=null){
//      imageBackdrop.setImageResource(R.mipmap.logo);
//    }

//    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//    fab.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View view) {
//        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//            .setAction("Action", null).show();
//
//        //TODO: connect to http://54.93.192.62:3000/maps
//
//        new GetMaps().execute("somedummyneverused URL");
//      }
//    });

    recyclerView = findViewById(com.digitalpointzero2.transportmaps.R.id.maps_list);
    assert recyclerView != null;

    if (findViewById(com.digitalpointzero2.transportmaps.R.id.maps_detail_container) != null) {
      // The detail container view will be present only in the
      // large-screen layouts (res/values-w900dp).
      // If this view is present, then the
      // activity should be in two-pane mode.
      mTwoPane = true;
    }
  }

  public static Context getContext(){
    return mContext;
  }

  private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
//    Log.d("Timing",String.valueOf(System.currentTimeMillis()));
    MapsContent maps = new MapsContent();

//    Log.d("Timing",String.valueOf(System.currentTimeMillis()));
    mAdapter = new SimpleItemRecyclerViewAdapter(maps.ITEMS);
    recyclerView.setAdapter(mAdapter);
  }

  //TODO add fillData to above setupRecyclerView
  private void fillData() {
//    adapter = new SimpleCursorAdapter(this, R.layout.todo_row, null, from,
//        to, 0);
//
//    setListAdapter(adapter);
  }

  @Override
  public void onStart(){
    super.onStart();
    //new EULA(this).show();

    setupRecyclerView((RecyclerView) recyclerView);
  }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_mapslist, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mAdapter.getFilter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return true;
            }
        });

        return true;
    }

    public class SimpleItemRecyclerViewAdapter
      extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder>
      implements Filterable{

    private final List<MapsContent.TransportMap> mValues;
    private List<MapsContent.TransportMap> mValuesFiltered;

    public SimpleItemRecyclerViewAdapter(List<MapsContent.TransportMap> items) {
      mValues = items;
        mValuesFiltered = mValues;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext())
          .inflate(com.digitalpointzero2.transportmaps.R.layout.maps_list_item, parent, false);

      return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
      holder.mItem = mValuesFiltered.get(position);
      holder.mIdView.setText(mValuesFiltered.get(position).id);
      holder.mContentView.setText(mValuesFiltered.get(position).country);

      holder.mView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if (mTwoPane) {
            Bundle arguments = new Bundle();
            arguments.putString(MapsDetailFragment.ARG_ITEM_ID, holder.mItem.id);
            MapsDetailFragment fragment = new MapsDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                .replace(com.digitalpointzero2.transportmaps.R.id.maps_detail_container, fragment)
                .commit();
          } else {
            Context context = v.getContext();
            Intent intent = new Intent(context, MapsDetailActivity.class);
            intent.putExtra(MapsDetailFragment.ARG_ITEM_ID, holder.mItem.id);

            context.startActivity(intent);
          }
        }
      });
    }

    @Override
    public int getItemCount() {
      return mValuesFiltered.size();
    }

    @Override
    public Filter getFilter() {
//        return null;
        return new Filter() {
          @Override
          protected FilterResults performFiltering(CharSequence charSequence) {
            String charString = charSequence.toString();
            if (charString.isEmpty()) {
              mValuesFiltered = mValues;
            } else {
              List<MapsContent.TransportMap> filteredList = new ArrayList<>();
              for (MapsContent.TransportMap row : mValues) {

                // name match condition. this might differ depending on your requirement
                // here we are looking for name or phone number match
                if (row.id.toLowerCase().contains(charString.toLowerCase())
//                        ||  row.country.contains(charSequence)
                        ) {
                  filteredList.add(row);
                }
              }

              mValuesFiltered = filteredList;
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = mValuesFiltered;
            return filterResults;
          }

          @Override
          protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            mValuesFiltered = (ArrayList<MapsContent.TransportMap>) filterResults.values;
            notifyDataSetChanged();
          }
        };
//      }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
      public final View mView;
      public final TextView mIdView;
      public final TextView mContentView;
      public MapsContent.TransportMap mItem;

      public ViewHolder(View view) {
        super(view);
        mView = view;
        mIdView = (TextView) view.findViewById(com.digitalpointzero2.transportmaps.R.id.map_name);
        mContentView = (TextView) view.findViewById(com.digitalpointzero2.transportmaps.R.id.country_name);
      }

      @Override
      public String toString() {
        return super.toString() + " '" + mContentView.getText() + "'";
      }
    }




//    private Cursor getMaps() {
//      // Run query
//      Uri uri = MapsContentProvider.CONTENT_URI;
////      String[] projection = new String[] { ContactsContract.Contacts._ID,
////        ContactsContract.Contacts.DISPLAY_NAME };
////      String selection = ContactsContract.Contacts.IN_VISIBLE_GROUP + " = '"
////          + ("1") + "'";
//      String[] selectionArgs = null;
////      String sortOrder = ContactsContract.Contacts.DISPLAY_NAME
////          + " COLLATE LOCALIZED ASC";
//
//      // TODO replace with managerQuery with Loader
//      return managedQuery(uri, projection, selection, selectionArgs,
//          sortOrder);
//    }



  }






}