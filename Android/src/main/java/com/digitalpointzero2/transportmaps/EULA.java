package com.digitalpointzero2.transportmaps;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by scjb on 31/8/16.
 */
class EULA {
  private String EULA_PREFIX = "eula";
  private AppCompatActivity mContext;

  EULA(AppCompatActivity context) {
    mContext = context;
  }

  private PackageInfo getPackageInfo() {
    PackageInfo info = null;
    try {
      info = mContext.getPackageManager().getPackageInfo(
          mContext.getPackageName(), PackageManager.GET_ACTIVITIES);
    } catch (PackageManager.NameNotFoundException e) {
      e.printStackTrace();
    }
    return info;
  }

  void show() {
    PackageInfo versionInfo = getPackageInfo();

    // The eulaKey changes every time you increment the version number in
    // the AndroidManifest.xml
    final String eulaKey = EULA_PREFIX + versionInfo.versionCode;
    final SharedPreferences prefs = PreferenceManager
        .getDefaultSharedPreferences(mContext);

    boolean bAlreadyAccepted = prefs.getBoolean(eulaKey, false);
    if (!bAlreadyAccepted) {

      // EULA title
      String title = mContext.getString(com.digitalpointzero2.transportmaps.R.string.app_name) + " v"
      + versionInfo.versionName;

      // EULA text
      String message = mContext.getString(com.digitalpointzero2.transportmaps.R.string.eula_string);

      // Disable orientation changes, to prevent parent activity
      // reinitialization
      mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

      AlertDialog.Builder builder = new AlertDialog.Builder(mContext)
          .setTitle(title)
          .setMessage(message)
          .setCancelable(false)
          .setPositiveButton(com.digitalpointzero2.transportmaps.R.string.accept,
              new Dialog.OnClickListener() {

                @Override
                public void onClick(
                    DialogInterface dialogInterface, int i) {
                  // Mark this version as read.
                  SharedPreferences.Editor editor = prefs
                      .edit();
                  editor.putBoolean(eulaKey, true);
                  editor.apply();

                  // Close dialog
                  dialogInterface.dismiss();

                  // Enable orientation changes based on
                  // device's sensor
                  mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
                }
              })
          .setNegativeButton(android.R.string.cancel,
              new Dialog.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog,
                                    int which) {
                  // Close the activity as they have declined
                  // the EULA
                  mContext.finish();
                  mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
                }

              });
      builder.create().show();
    }
  }

}
