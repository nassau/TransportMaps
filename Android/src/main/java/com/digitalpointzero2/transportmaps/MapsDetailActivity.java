package com.digitalpointzero2.transportmaps;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.app.NavUtils;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;

/**
 * An activity representing a single maps detail screen. This
 * activity is only used narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link MapsListActivity}.
 */
public class MapsDetailActivity extends AppCompatActivity {

  MapsDetailFragment mFragment = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(com.digitalpointzero2.transportmaps.R.layout.activity_maps_detail);

    Toolbar toolbar = (Toolbar) findViewById(com.digitalpointzero2.transportmaps.R.id.detail_toolbar);
    //TODO: Add copyright to maps by displaying an overflow button that allows showing a copyright notice
    // See this: https://developer.android.com/training/appbar/actions.html

    setSupportActionBar(toolbar);



//    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//    fab.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View view) {
//        Snackbar.make(view, "TODO: Search for a station via OCR, or indexed data", Snackbar.LENGTH_LONG)
//            .setAction("Action", null).show();
//      }
//    });

    // Show the Up button in the action bar.
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.setDisplayHomeAsUpEnabled(true);
    }

    // savedInstanceState is non-null when there is fragment state
    // saved from previous configurations of this activity
    // (e.g. when rotating the screen from portrait to landscape).
    // In this case, the fragment will automatically be re-added
    // to its container so we don't need to manually add it.
    // For more information, see the Fragments API guide at:
    //
    // http://developer.android.com/guide/components/fragments.html
    //
    if (savedInstanceState != null) {
      //Restore the fragment's instance
      mFragment = (MapsDetailFragment) getSupportFragmentManager().getFragment(savedInstanceState, "mFragment");
    }
    else {
      // Create the detail fragment and add it to the activity
      // using a fragment transaction.
      Bundle arguments = new Bundle();
      arguments.putString(MapsDetailFragment.ARG_ITEM_ID,
          getIntent().getStringExtra(MapsDetailFragment.ARG_ITEM_ID));
      mFragment = new MapsDetailFragment();
      mFragment.setArguments(arguments);

      getSupportFragmentManager().beginTransaction()
          .add(com.digitalpointzero2.transportmaps.R.id.maps_detail_container, mFragment)
          .commit();
    }

//TODO for the MapsContentProvider
//
//      Bundle extras = getIntent().getExtras();
//
//      // check from the saved Instance
//      todoUri = (bundle == null) ? null : (Uri) bundle
//          .getParcelable(MyTodoContentProvider.CONTENT_ITEM_TYPE);
//
//      // Or passed from the other activity
//      if (extras != null) {
//        todoUri = extras
//            .getParcelable(MyTodoContentProvider.CONTENT_ITEM_TYPE);
//
//        fillData(todoUri);
//      }





  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    //Save the fragment's instance
    getSupportFragmentManager().putFragment(outState, "mFragment", mFragment);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
//      case R.id.action_license:
//        // User chose the "Licence" item, show map licence...
//        Snackbar.make(findViewById(R.id.detail_toolbar), "blah", Snackbar.LENGTH_LONG)
//          .setAction("Action", null).show();
//        return true;

      case com.digitalpointzero2.transportmaps.R.id.home:
        // This ID represents the Home or Up button. In the case of this
        // activity, the Up button is shown. Use NavUtils to allow users
        // to navigate up one level in the application structure. For
        // more details, see the Navigation pattern on Android Design:
        //
        // http://developer.android.com/design/patterns/navigation.html#up-vs-back
        //
        NavUtils.navigateUpTo(this, new Intent(this, MapsListActivity.class));
        return true;

      default:
        // If we got here, the user's action was not recognized.
        // Invoke the superclass to handle it.
        return super.onOptionsItemSelected(item);

    }
  }
}
